//
//  AppDelegate.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

  lazy var window: UIWindow? = .init(frame: UIScreen.main.bounds)

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

    window?.rootViewController = home()
    window?.makeKeyAndVisible()

    return true
  }

  private func home() -> UINavigationController? {
    let homeStoryboard = UIStoryboard(name: "HomeView", bundle: nil)
    guard let homeViewController = homeStoryboard.instantiateInitialViewController() as? HomeViewController else {
      fatalError("Unable to Instantiate Home View Controller")
    }
    return UINavigationController(rootViewController: homeViewController)
  }
}
