//
//  ScoreboardRouter.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import Foundation
import UIKit

protocol ScoreboardRoutingLogic: AnyObject {
  func routeToHome()
}

protocol ScoreboardDataPassing: AnyObject {
  var dataStore: ScoreboardDataStore? { get }
}

final class ScoreboardRouter: ScoreboardRoutingLogic, ScoreboardDataPassing {

  weak var viewController: ScoreboardViewController?
  var dataStore: ScoreboardDataStore?

  func routeToHome() {
    viewController?.navigationController?.popToRootViewController(animated: true)
  }
}
