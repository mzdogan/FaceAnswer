//
//  ScoreboardViewController.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import UIKit
import CoreMedia

protocol ScoreboardDisplayLogic: AnyObject {
  
  func displayScores(viewModel: Scoreboard.CoreScoreData.ViewModel)
  func displayGameScore(score: String)
}

final class ScoreboardViewController: UIViewController {

  var interactor: ScoreboardBusinessLogic?
  var router: (ScoreboardRoutingLogic & ScoreboardDataPassing)?

  private var userScores: Scoreboard.CoreScoreData.ViewModel = .empty {
    didSet {
      tableView.reloadData()
    }
  }

  // MARK: - IBOutlets

  @IBOutlet weak var usernameTextField: UITextField!
  @IBOutlet weak var saveButton: Button!
  @IBOutlet weak var pointLabel: UILabel!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var scoreboardButton: Button!


  // MARK: - Object lifecycle

  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    registerCells()
    configureNavigationBar()
    setTextFieldDelegate()
    buttonConfigurations()
    hideKeyboardWhenTappedAround()

    interactor?.showGameScore()
    interactor?.showScoreboard()
  }

  // MARK: - Setup

  private func setup() {
    let viewController = self

    let interactor = ScoreboardInteractor()
    let presenter = ScoreboardPresenter()
    let worker = ScoreboardWorker()
    let router = ScoreboardRouter()

    viewController.interactor = interactor
    viewController.router = router
    interactor.presenter = presenter
    interactor.worker = worker
    presenter.viewController = viewController
    router.viewController = viewController
    router.dataStore = interactor
  }

 private func registerCells() {
    tableView.register(UINib(nibName: ScoreboardTableViewCell.identifer, bundle: nil), forCellReuseIdentifier: ScoreboardTableViewCell.identifer)
  }

}

extension ScoreboardViewController: ScoreboardDisplayLogic {

  func displayScores(viewModel:Scoreboard.CoreScoreData.ViewModel) {
    userScores = viewModel
  }

  func displayGameScore(score: String) {
    pointLabel.text = "\(score)pt"
  }
}
// MARK: - UITableViewDataSource, UITableViewDelegate

extension ScoreboardViewController: UITableViewDataSource, UITableViewDelegate {

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return userScores.scores.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: ScoreboardTableViewCell.identifer, for: indexPath) as! ScoreboardTableViewCell
    let score = userScores.scores[indexPath.row]
    cell.configure(scores: score, index: indexPath.row)
    return cell
  }
}

// MARK: - Button & Keyboard & NavigationBar

extension ScoreboardViewController {

  // MARK: - Button

  private func buttonConfigurations() {
    saveButton.addTarget(self, action: #selector(savePressed), for: .touchUpInside)
    scoreboardButton.addTarget(self, action: #selector(scoreboardPressed) , for: .touchUpInside)
    saveButtonDisabled()
  }

  @objc private func scoreboardPressed() {
    tableView.isHidden = false
    tableView.reloadData()
    scoreboardButtonDisabled()
  }

  @objc private func savePressed() {
    SoundService.shared.playSoundEffect(filename: "fa-buttonPressed")

    guard let username = usernameTextField.text else { return }
    interactor?.setScore(username: username)
    saveButton.isHidden = true

    scoreSaved()
  }

  func scoreSaved() {
    usernameTextField.text = "Your score is saved!"
    usernameTextField.textColor = Colors.white
    usernameTextField.backgroundColor = .systemGreen
    usernameTextField.isUserInteractionEnabled = false
  }

  // MARK: - Button Disable

  private func saveButtonDisabled() {
    saveButton.isUserInteractionEnabled = false
    saveButton.setEnabled(false)
  }

  private func scoreboardButtonDisabled() {
    scoreboardButton.isUserInteractionEnabled = false
    scoreboardButton.setEnabled(false)
  }

  // MARK: - Navigation Bar

  func configureNavigationBar(){
    navigationItem.leftBarButtonItem = UIBarButtonItem(
      image: #imageLiteral(resourceName: "ic-home"),
      style: .plain,
      target: self,
      action: #selector(homePressed))
   navigationController?.navigationBar.tintColor = Colors.white
  }

  @objc private func homePressed() {
    SoundService.shared.playSoundEffect(filename: "fa-buttonPressed")
    router?.routeToHome()
  }

}

// MARK: - UITextFieldDelegate

extension ScoreboardViewController: UITextFieldDelegate {

  private func setTextFieldDelegate() {
    usernameTextField.delegate = self
  }

  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }

  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

    let oldText = textField.text!
    let text = oldText.replacingCharacters(in: Range(range, in: oldText)!, with: string)

    let newLength = text.count

    let isSmallerThanTwo = newLength < 2
    let hasEmoji = text.containsEmoji

    if !isSmallerThanTwo && !hasEmoji {
      saveButton.isUserInteractionEnabled = true
      saveButton.setEnabled(true)
    }
    else {
      saveButtonDisabled()
    }
    return  newLength <= 10
  }
}

// MARK: - Keyboard

extension ScoreboardViewController {

 private func hideKeyboardWhenTappedAround() {
    let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
    tap.cancelsTouchesInView = false
    view.addGestureRecognizer(tap)
  }

  @objc private func dismissKeyboard() {
    view.endEditing(true)
  }
}

