//
//  ScoreboardWorker.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import Foundation

protocol ScoreboardWorkerProtocol { }

final class ScoreboardWorker {
  var scoreboardProtocol: ScoreboardWorkerProtocol?
}
