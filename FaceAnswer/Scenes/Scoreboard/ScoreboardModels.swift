//
//  ScoreboardModels.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import Foundation

enum Scoreboard {

  enum CoreScoreData {

    struct Request { }

    struct Response { }

    struct UserScore {
      var scores: [Score]
    }

    struct Score {
      let username: String
      let point: String
    }

    struct ViewModel {
      let scores: [Score]

      static let empty = ViewModel(scores: [])
    }
  }
}
