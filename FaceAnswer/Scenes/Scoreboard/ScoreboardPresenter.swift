//
//  ScoreboardPresenter.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import Foundation

protocol ScoreboardPresentationLogic: AnyObject {
  func presentScores(scores: [Scoreboard.CoreScoreData.Score])
  func presentGameScore(point: Int)

}

final class ScoreboardPresenter: ScoreboardPresentationLogic {

  weak var viewController: ScoreboardDisplayLogic?

  func presentScores(scores: [Scoreboard.CoreScoreData.Score]) {
    let viewModel = Scoreboard.CoreScoreData.ViewModel(scores: scores)
    viewController?.displayScores(viewModel: viewModel)
  }

  func presentGameScore(point: Int) {
    let score = String(point)
    viewController?.displayGameScore(score: score)
  }
}
