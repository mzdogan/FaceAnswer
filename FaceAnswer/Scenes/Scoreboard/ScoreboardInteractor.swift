//
//  ScoreboardInteractor.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import Foundation

protocol ScoreboardBusinessLogic: AnyObject {
  func setScore(username: String)
  func showGameScore()
  func showScoreboard()
}

protocol ScoreboardDataStore: AnyObject {
  var point: Int { get set }
}

class ScoreboardInteractor: ScoreboardBusinessLogic, ScoreboardDataStore {

  var presenter: ScoreboardPresentationLogic?
  var worker: ScoreboardWorker?

  var point: Int = 0

  func showScoreboard() {
    let scores =  ScoreboardService.service.showScore()
    presenter?.presentScores(scores: scores)
  }

  func setScore(username: String) {
  ScoreboardService.service.setScore(username: username, point: String(point))
   showScoreboard()
  }

  func showGameScore() {
    presenter?.presentGameScore(point: point)
  }
}
