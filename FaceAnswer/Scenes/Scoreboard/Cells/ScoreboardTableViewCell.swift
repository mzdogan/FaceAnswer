//
//  ScoreboardTableViewCell.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 23.05.2022.
//

import UIKit

class ScoreboardTableViewCell: UITableViewCell {

  static let identifer = "ScoreboardTableViewCell"

  // MARK: - IBOutlets
  
  @IBOutlet weak var usernameLabel: UILabel!
  @IBOutlet weak var pointLabel: UILabel!
  @IBOutlet weak var highlightView: UIView!

  func configure(scores: Scoreboard.CoreScoreData.Score, index: Int?) {
    usernameLabel.text = scores.username
    pointLabel.text = scores.point
    highlightView.backgroundColor = index == 0 ? Colors.highlighted : .clear
  }
}
