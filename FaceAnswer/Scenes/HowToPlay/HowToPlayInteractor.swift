//
//  HowToPlayInteractor.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 24.05.2022.
//

import Foundation

protocol HowToPlayBusinessLogic: AnyObject { }

protocol HowToPlayDataStore: AnyObject { }

class HowToPlayInteractor: HowToPlayBusinessLogic, HowToPlayDataStore {

  var presenter: HowToPlayPresentationLogic?
  var worker: HowToPlayWorker?
}
