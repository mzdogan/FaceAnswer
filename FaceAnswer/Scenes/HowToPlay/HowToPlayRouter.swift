//
//  HowToPlayRouter.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 24.05.2022.
//

import Foundation
import UIKit

protocol HowToPlayRoutingLogic: AnyObject {
  func routeToHome()
}

protocol HowToPlayDataPassing: AnyObject {
  var dataStore: HowToPlayDataStore? { get }
}

final class HowToPlayRouter: HowToPlayRoutingLogic, HowToPlayDataPassing {

  weak var viewController: HowToPlayViewController?
  var dataStore: HowToPlayDataStore?

  func routeToHome() {
    let storyboard = UIStoryboard(name: "HomeView", bundle: nil)
    if let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeView") as? HomeViewController {
      viewController?.navigationController?.pushViewController(homeViewController, animated: true)
    }
  }
}
