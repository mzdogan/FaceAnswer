//
//  HowToPlayModels.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 24.05.2022.
//

import Foundation

enum HowToPlay {

  enum UseCase {

    struct Request { }

    struct Response { }

    struct ViewModel { }
  }
}
