//
//  HowToPlayPresenter.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 24.05.2022.
//

import Foundation

protocol HowToPlayPresentationLogic: AnyObject { }

final class HowToPlayPresenter: HowToPlayPresentationLogic {

  weak var viewController: HowToPlayDisplayLogic?
}
