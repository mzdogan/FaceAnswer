//
//  HowToPlayViewController.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 24.05.2022.
//

import UIKit

protocol HowToPlayDisplayLogic: AnyObject { }

final class HowToPlayViewController: UIViewController {

  var interactor: HowToPlayBusinessLogic?
  var router: (HowToPlayRoutingLogic & HowToPlayDataPassing)?

  // MARK: - IBOutlets

  @IBOutlet weak var gifImageView: UIImageView!

  // MARK: - Object lifecycle

  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    displayGif()
    configureNavigationBar()
  }

  // MARK: - Setup

  private func setup() {
    let viewController = self
    let interactor = HowToPlayInteractor()
    let presenter = HowToPlayPresenter()
    let worker = HowToPlayWorker()
    let router = HowToPlayRouter()
    viewController.interactor = interactor
    viewController.router = router
    interactor.presenter = presenter
    interactor.worker = worker
    presenter.viewController = viewController
    router.viewController = viewController
    router.dataStore = interactor
  }

  func displayGif() {
    gifImageView.loadGif(name: "Gif-Owl")
  }

  // MARK: - Navigation Bar

  func configureNavigationBar() {
    navigationItem.leftBarButtonItem = UIBarButtonItem(
      image: #imageLiteral(resourceName: "ic-home"),
      style: .plain,
      target: self,
      action: #selector(homePressed))
    navigationController?.navigationBar.tintColor = Colors.white
  }

  @objc private func homePressed() {
    SoundService.shared.playSoundEffect(filename: "fa-buttonPressed")
    router?.routeToHome()
  }
}

// MARK: - HowToPlayDisplayLogic

extension HowToPlayViewController: HowToPlayDisplayLogic { }
