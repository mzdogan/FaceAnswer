//
//  HomeInteractor.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import Foundation

protocol HomeBusinessLogic: AnyObject {
 func setCategory(category: String)
}

protocol HomeDataStore: AnyObject {
  var category: String { get set }
}

class HomeInteractor: HomeBusinessLogic, HomeDataStore {

  var presenter: HomePresentationLogic?
  var worker: HomeWorker?

  var category: String = ""

  func setCategory(category: String) {
    self.category = category
  }
}
