//
//  HomeRouter.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import Foundation
import UIKit

protocol HomeRoutingLogic: AnyObject {
  func routeToQuiz()
  func routeToHowToPlay()
}

protocol HomeDataPassing: AnyObject {
  var dataStore: HomeDataStore? { get }
}

final class HomeRouter: HomeRoutingLogic, HomeDataPassing {

  weak var viewController: HomeViewController?
  var dataStore: HomeDataStore?

  func routeToQuiz() {
    let storyboard = UIStoryboard(name: "QuizView", bundle: nil)
    if let quizViewController = storyboard.instantiateViewController(withIdentifier: "QuizView") as? QuizViewController {
      quizViewController.router?.dataStore?.category = dataStore?.category ?? Categories.Random.rawValue
      viewController?.navigationController?.pushViewController(quizViewController, animated: true)
    }
  }

  func routeToHowToPlay() {
    let storyboard = UIStoryboard(name: "HowToPlayView", bundle: nil)
    if let howToPlayViewController = storyboard.instantiateViewController(withIdentifier: "HowToPlayView") as? HowToPlayViewController {
      viewController?.navigationController?.pushViewController(howToPlayViewController, animated: true)
    }
  }
}
