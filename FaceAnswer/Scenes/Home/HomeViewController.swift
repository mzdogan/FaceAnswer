//
//  HomeViewController.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import AVFoundation
import UIKit

enum Categories: String {
  case Random
  case Movie
  case Music
  case History
  case Codeway

  static func categoryByTag(_ tag: Int) -> String {
    switch tag {
    case 1: return Categories.Random.rawValue
    case 2: return Categories.Movie.rawValue
    case 3: return Categories.Music.rawValue
    case 4: return Categories.History.rawValue
    case 5: return Categories.Codeway.rawValue
    default: return ""
    }
  }
}

protocol HomeDisplayLogic: AnyObject { }

final class HomeViewController: UIViewController {

  var interactor: HomeBusinessLogic?
  var router: (HomeRoutingLogic & HomeDataPassing)?

  // MARK: - IBOutlets

  @IBOutlet weak var howToPlayButton: UIButton!
  @IBOutlet weak var randomButton: UIButton!
  @IBOutlet weak var movieButton: UIButton!
  @IBOutlet weak var musicButton: UIButton!
  @IBOutlet weak var historyButton: UIButton!
  @IBOutlet weak var codewayButton: UIButton!

  // MARK: - Object lifecycle

  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    buttonConfigurations()
    configureNavigationBar()
  }

  // MARK: - Setup

  private func setup() {
    let viewController = self

    let interactor = HomeInteractor()
    let presenter = HomePresenter()
    let worker = HomeWorker()
    let router = HomeRouter()

    viewController.interactor = interactor
    viewController.router = router
    interactor.presenter = presenter
    interactor.worker = worker
    presenter.viewController = viewController
    router.viewController = viewController
    router.dataStore = interactor
  }

  // MARK: - Button

  @IBAction func categoryButtonPressed(_ sender: UIButton) {
    SoundService.shared.playSoundEffect(filename: "fa-buttonPressed")

    interactor?.setCategory(category: Categories.categoryByTag(sender.tag))
    router?.routeToQuiz()
  }

  private func buttonConfigurations() {
    howToPlayButton.addTarget(self, action: #selector( howToPlayPressed), for: .touchUpInside)
  }
  
  @objc private func howToPlayPressed() {
    SoundService.shared.playSoundEffect(filename: "fa-buttonPressed")
    router?.routeToHowToPlay()
  }

  // MARK: - Navigation Bar

  func configureNavigationBar() {
    navigationItem.setHidesBackButton(true, animated: false)
  }
}

// MARK: - HomeDisplayLogic

extension HomeViewController: HomeDisplayLogic { }

