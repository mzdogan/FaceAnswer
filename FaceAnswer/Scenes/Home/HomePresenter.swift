//
//  HomePresenter.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import Foundation

protocol HomePresentationLogic: AnyObject { }

final class HomePresenter: HomePresentationLogic {

  weak var viewController: HomeDisplayLogic?
}
