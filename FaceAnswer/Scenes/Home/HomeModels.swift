//
//  HomeModels.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import Foundation

enum Home {

  enum UseCase {

    struct Request { }

    struct Response { }

    struct ViewModel { }
  }
}
