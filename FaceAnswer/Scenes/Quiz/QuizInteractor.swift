//
//  QuizInteractor.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import Foundation
import CoreAudio
import UIKit

protocol QuizBusinessLogic: AnyObject {
  func selectedChoice(choiceIndex: Int)
  func fetchQuizContent()
  func fetchNextQuestion()
  func revealAnswer()
}

protocol QuizDataStore: AnyObject {

  var resultResponse: [Quiz.FetchQuiz.QnA] { get set }

  var isChoiceCorrect: Bool { get set }

  var questionIndex: Int { get set }
  var point: Int { get set }
  var maxQuestion: Int { get set }
  var correctChoiceIndex: Int { get set }
  var category: String { get set }
}

class QuizInteractor: QuizBusinessLogic, QuizDataStore {

  var presenter: QuizPresentationLogic?
  var worker: QuizWorker?

  var isChoiceCorrect: Bool = false

  var resultResponse: [Quiz.FetchQuiz.QnA] = []

  var questionIndex: Int = 0
  var point: Int = 0
  var maxQuestion: Int = 10
  var correctChoiceIndex: Int = 0
  var category: String = ""

  func fetchQuizContent() {

    guard let quizContent =  worker?.fetchQuiz(fileName: category) else { return }
    
    let response = Quiz.FetchQuiz.Response(data: quizContent)
    let shuffledResponse = response.data.shuffled()
    resultResponse = Array(shuffledResponse.prefix(maxQuestion))

    presenter?.presentQuizContent(responseData: resultResponse,
                                  index: questionIndex,
                                  point: point,
                                  category: category)
  }

  func selectedChoice(choiceIndex: Int) {
    isChoiceCorrect = resultResponse[questionIndex].choices[choiceIndex].isCorrect
    point = isChoiceCorrect ? point + 10 : point
    presenter?.presentAnswer(choiceIndex: choiceIndex,
                             isCorrect: isChoiceCorrect,
                             point: point)
  }

  func fetchNextQuestion() {
    questionIndex += 1

    if questionIndex < resultResponse.count {
      presenter?.presentQuizContent(responseData: resultResponse,
                                    index: questionIndex,
                                    point: point,
                                    category: category)
    }
    else {
      presenter?.presentScore()
    }
  }

  func revealAnswer() {

    if resultResponse[questionIndex].choices[0].isCorrect {
      correctChoiceIndex = 0
    }
    else {
      correctChoiceIndex = 1
    }
    presenter?.presentRevealedAnswer(correctChoiceIndex: correctChoiceIndex, point: point)
  }
}
