//
//  QuizViewController.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import AVFoundation
import UIKit
import Vision

protocol QuizDisplayLogic: AnyObject {

  func displayQuizContent(viewModel: Quiz.FetchQuiz.ViewModel, index: Int,
                          point: Int, category: String)

  func displayAnswer(buttonColor: UIColor, choiceIndex: Int,
                     point: Int, soundName: String)

  func displayScore()
}

final class QuizViewController: UIViewController {

  var interactor: QuizBusinessLogic?
  var router: (QuizRoutingLogic & QuizDataPassing)?

  // MARK: - IBOutlets

  @IBOutlet weak var pointLabel: UILabel!
  @IBOutlet weak var timerLabel: UILabel!
  @IBOutlet weak var questionLabel: UILabel!
  @IBOutlet weak var categoryLabel: UILabel!

  @IBOutlet weak var leftButton: UIButton!
  @IBOutlet weak var rightButton: UIButton!

  // MARK: - Face Detection Properties

  private var sequenceRequestHandler = VNSequenceRequestHandler()
  private let captureSession = AVCaptureSession()
  private lazy var previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
  private let videoDataOutput = AVCaptureVideoDataOutput()

  private var timer: Timer?

  private var questions: [Quiz.FetchQuiz.QnA] = []

  private var leftButtonIndex = 0
  private var rightButtonIndex = 1

  private var canAnswer = true

  // MARK: - Object lifecycle

  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    setupCamera()
    configureNavigationBar()
    captureSession.startRunning()
    interactor?.fetchQuizContent()
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    timer?.invalidate()
  }

  // MARK: - Setup

  private func setup() {
    let viewController = self

    let interactor = QuizInteractor()
    let presenter = QuizPresenter()
    let worker = QuizWorker()
    let router = QuizRouter()

    viewController.interactor = interactor
    viewController.router = router
    interactor.presenter = presenter
    interactor.worker = worker
    presenter.viewController = viewController
    router.viewController = viewController
    router.dataStore = interactor
  }

  // MARK: - Navigation Bar

  func configureNavigationBar() {
    navigationItem.setHidesBackButton(true, animated: false)
  }

  func resetButtons() {
    leftButton.backgroundColor = Colors.white
    rightButton.backgroundColor = Colors.white
  }
}

// MARK: - QuizDisplayLogic

extension QuizViewController: QuizDisplayLogic {

  func displayQuizContent(viewModel: Quiz.FetchQuiz.ViewModel, index: Int, point: Int, category: String) {

    let currentQuestion = viewModel.quizContent[index]

    resetButtons()
    startCounter()

    categoryLabel.text = category
    questionLabel.text = currentQuestion.question
    pointLabel.text = "\(point) pt"

    leftButton.setTitle(currentQuestion.choices[leftButtonIndex].choice, for: .normal)
    rightButton.setTitle(currentQuestion.choices[rightButtonIndex].choice, for: .normal)

    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
      self?.canAnswer = true
    }
  }

  func displayAnswer(buttonColor: UIColor, choiceIndex: Int, point: Int, soundName: String) {

    if choiceIndex == 0 {
      DispatchQueue.main.async { [weak self] in
        self?.leftButton.backgroundColor = buttonColor
        self?.pointLabel.text = "\(point) pt"
        self?.timer?.invalidate()
      }
      SoundService.shared.playSoundEffect(filename: soundName)
    }
    else {
      DispatchQueue.main.async { [weak self] in
        self?.rightButton.backgroundColor = buttonColor
        self?.pointLabel.text = "\(point) pt"
        self?.timer?.invalidate()
      }
      SoundService.shared.playSoundEffect(filename: soundName)
    }

    let seconds = 1.0
    DispatchQueue.main.asyncAfter(deadline: .now() + seconds) { [weak self] in
      self?.interactor?.fetchNextQuestion()
    }
  }

  func displayScore() {
    SoundService.shared.playSoundEffect(filename: "fa-scoreboard")
    router?.routeToScoreboard()
  }
}

// MARK: - Timer

private extension QuizViewController {

  func startCounter() {

    let timerInitialValue = 15
    var count = timerInitialValue
    var text: String?
    var textColor = UIColor.black

    timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] timer in

      if count == 0 {
        self?.interactor?.revealAnswer()
        timer.invalidate()
      }
      else {
        let isOneDigit = count < 10
        let format = isOneDigit ? "00:0%d" : "00:%d"

        text = String(format: format, count)
        textColor = isOneDigit ? UIColor.red : Colors.black
        count -= 1
      }
      self?.timerLabel.text = text
      self?.timerLabel.textColor = textColor
    }
  }
}

// MARK: - AVCaptureVideo + Face Tilt Detection

extension QuizViewController {

  private func setupCamera() {
    let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .front)
    if let device = deviceDiscoverySession.devices.first {
      if let deviceInput = try? AVCaptureDeviceInput(device: device) {
        if captureSession.canAddInput(deviceInput) {
          captureSession.addInput(deviceInput)

          setupPreview()
        }
      }
    }
  }

  private func setupPreview() {
    previewLayer.videoGravity = .resizeAspectFill
    previewLayer.frame = view.bounds
    view.layer.insertSublayer(previewLayer, at: 0)

    videoDataOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString) : NSNumber(value: kCVPixelFormatType_32BGRA)] as [String : Any]

    videoDataOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "Video Queue"))
    captureSession.addOutput(videoDataOutput)

    let videoConnection = videoDataOutput.connection(with: .video)
    videoConnection?.videoOrientation = .portrait
  }
}

extension QuizViewController: AVCaptureVideoDataOutputSampleBufferDelegate {

  func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {

    guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }

    if canAnswer {
      let detectionRequest = VNDetectFaceLandmarksRequest(completionHandler: { [weak self] (request: VNRequest, error: Error?) in
        guard let observationResults = request.results as? [VNFaceObservation],
              let result = observationResults.first else { return }
        self?.detectFaceTilt(result: result)
      })

      do {
        try sequenceRequestHandler.perform([detectionRequest],
                                           on: imageBuffer,
                                           orientation: .leftMirrored)
      }
      catch {
        print(error.localizedDescription)
      }
    }
  }

  private func detectFaceTilt(result: VNFaceObservation) {

    let yawAngle = (result.yaw ?? 0.0).doubleValue

    switch yawAngle {
    case 0.0:
      break
      
    case -1.0 ... -0.20:
      canAnswer = false
      leftButtonSelected()

    case 0.20 ... 1.0:
      canAnswer = false
      rightButtonSelected()

    default:
      break
    }
  }

  func rightButtonSelected() {
    interactor?.selectedChoice(choiceIndex: rightButtonIndex)
  }

  func leftButtonSelected() {
    interactor?.selectedChoice(choiceIndex: leftButtonIndex)
  }
}
