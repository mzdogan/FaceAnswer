//
//  QuizPresenter.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import Foundation
import UIKit

protocol QuizPresentationLogic: AnyObject {
  func presentQuizContent(responseData: [Quiz.FetchQuiz.QnA],
                          index : Int,
                          point: Int,
                          category: String)
  
  func presentAnswer(choiceIndex: Int, isCorrect: Bool, point: Int)
  func presentRevealedAnswer(correctChoiceIndex: Int, point: Int)
  func presentScore()

}

final class QuizPresenter: QuizPresentationLogic {

  weak var viewController: QuizDisplayLogic?

  private var soundName: String = ""
  private var buttonColor: UIColor = Colors.white


  func presentQuizContent(responseData: [Quiz.FetchQuiz.QnA], index : Int, point: Int, category: String) {
    let viewModel = Quiz.FetchQuiz.ViewModel(quizContent: responseData)
    viewController?.displayQuizContent(viewModel: viewModel, index: index, point: point, category: category)
  }

  func presentAnswer(choiceIndex: Int, isCorrect: Bool, point: Int) {
    buttonColor = isCorrect ? UIColor.green : UIColor.red
    soundName = isCorrect ? "fa-correctAnswer" : "fa-falseAnswer"
    viewController?.displayAnswer(buttonColor: buttonColor, choiceIndex: choiceIndex, point: point, soundName: soundName)
  }

  func presentRevealedAnswer(correctChoiceIndex: Int, point: Int) {
    buttonColor = UIColor.green
    soundName = "fa-timesUp"
    viewController?.displayAnswer(buttonColor: buttonColor, choiceIndex: correctChoiceIndex, point: point, soundName: soundName)
  }

  func presentScore() {
    viewController?.displayScore()
  }
}
