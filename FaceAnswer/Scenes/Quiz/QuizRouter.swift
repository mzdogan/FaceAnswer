//
//  QuizRouter.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import Foundation
import UIKit

protocol QuizRoutingLogic: AnyObject {
  func routeToScoreboard()
}

protocol QuizDataPassing: AnyObject {
  var dataStore: QuizDataStore? { get }
}

final class QuizRouter: QuizRoutingLogic, QuizDataPassing {

  weak var viewController: QuizViewController?
  var dataStore: QuizDataStore?

  func routeToScoreboard() {
    let storyboard = UIStoryboard(name: "ScoreboardView", bundle: nil)
    if let scoreboardViewController = storyboard.instantiateViewController(withIdentifier: "ScoreboardView") as? ScoreboardViewController {

      scoreboardViewController.router?.dataStore?.point = dataStore?.point ?? 0
      viewController?.navigationController?.pushViewController(scoreboardViewController, animated: true)
    }
  }
}
