//
//  QuizModels.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import Foundation

enum Quiz {

  enum FetchQuiz {

    struct Request { }

    struct Response: Codable {
      let data: [QnA]
    }

    struct QnA: Codable {
      let question: String
      let choices: [Choice]
    }

    struct Choice: Codable {
      let choice: String
      let isCorrect: Bool
    }

    struct ViewModel {
      let quizContent: [QnA]
    }
  }
}
