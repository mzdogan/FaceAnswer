//
//  QuizWorker.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 20.05.2022.
//

import Foundation

final class QuizWorker {
  func fetchQuiz(fileName: String) -> [Quiz.FetchQuiz.QnA]? {
    if let URL = Bundle.main.url(forResource: fileName + "JSON", withExtension: "json") {
      do {
        let data = try Data(contentsOf: URL)
        let decoder = JSONDecoder()
        let result = try decoder.decode(Quiz.FetchQuiz.Response.self, from: data)
        return result.data
      }
      catch {
        print(error.localizedDescription)   
      }
    }
    return nil
  }
}
