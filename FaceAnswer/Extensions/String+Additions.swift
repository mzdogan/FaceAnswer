//
//  String+Additions.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 24.05.2022.
//

import Foundation

extension String {

  var containsEmoji: Bool {
    guard let _ = unicodeScalars.first(where: { $0.properties.isEmoji }) else { return false }
    return true
  }
}
