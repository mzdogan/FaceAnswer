//
//  UIView+InterfaceBuilder.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 21.05.2022.
//

import UIKit

extension UIView {

  @IBInspectable var cornerRadius: CGFloat {
    get {
      return layer.cornerRadius
    }
    set {
      layer.cornerRadius = newValue
      layer.masksToBounds = newValue > 0
    }
  }

  @IBInspectable var shadowColor: UIColor? {
    get {
      if let color = layer.shadowColor {
        return UIColor(cgColor: color)
      }
      return nil
    }
    set {
      layer.shadowColor = newValue?.cgColor
    }
  }

  @IBInspectable var shadowOpacity: Float {
    get {
      return layer.shadowOpacity
    }
    set {
      layer.masksToBounds = false
      layer.shadowOpacity = newValue
    }
  }

  @IBInspectable var shadowOffset: CGPoint {
    get {
      return CGPoint(x: layer.shadowOffset.width, y: layer.shadowOffset.height)
    }
    set {
      layer.masksToBounds = false
      layer.shadowOffset = CGSize(width: newValue.x, height: newValue.y)
    }
  }

  @IBInspectable var shadowBlur: CGFloat {
    get {
      return layer.shadowRadius
    }
    set {
      layer.masksToBounds = false
      layer.shadowRadius = newValue
    }
  }

  @IBInspectable var borderWidth: CGFloat {
    get { return layer.borderWidth }
    set { layer.borderWidth = newValue }
  }

  @IBInspectable var borderColor: UIColor? {
    get {
      guard let borderColor = layer.borderColor else { return nil }
      return UIColor(cgColor: borderColor)
    }
    set {
      layer.borderColor = newValue?.cgColor
    }
  }
}
