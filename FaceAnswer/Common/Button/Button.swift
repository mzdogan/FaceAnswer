//
//  Button.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 23.05.2022.
//

import UIKit

class Button: UIButton {

  // MARK: - IBInspectable

  @IBInspectable var disabledColor: UIColor?
  @IBInspectable var enabledColor: UIColor?

  // MARK: - View's Lifecycle

  public override func awakeFromNib() {
    super.awakeFromNib()
    setBackgroundColor()
  }

  // MARK: - Preparation

  public func setEnabled(_ enabled: Bool) {
    isEnabled = enabled
    setBackgroundColor()
  }

  private func setBackgroundColor() {
    backgroundColor = isEnabled ? enabledColor : disabledColor
  }
}
