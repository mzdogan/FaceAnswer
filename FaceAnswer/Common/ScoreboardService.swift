//
//  ScoreboardService.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 24.05.2022.
//

import Foundation

class ScoreboardService {

  static let service = ScoreboardService()

  private var scores: [Scoreboard.CoreScoreData.Score] = []

  func setScore(username: String, point: String) {
    let newScore = Scoreboard.CoreScoreData.Score(username: username, point: point)
    scores.insert(newScore, at: 0)
  }

  func showScore() -> [Scoreboard.CoreScoreData.Score] {
    return scores
  }
}
