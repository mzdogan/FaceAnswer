//
//  SoundService.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 22.05.2022.
//

import AVFoundation

class SoundService {

  static let shared = SoundService()

  private var audioPlayer: AVAudioPlayer?

  func playSoundEffect(filename: String) {
    guard let URL = Bundle.main.url(forResource: filename, withExtension: "wav") else { return }

    do {
      audioPlayer = try AVAudioPlayer(contentsOf: URL)
      audioPlayer?.play()
    }
    catch {
      print(error.localizedDescription)
    }
  }
}
