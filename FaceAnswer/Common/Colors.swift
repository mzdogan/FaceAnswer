//
//  Colors.swift
//  FaceAnswer
//
//  Created by Mehmet Zahid Dogan on 23.05.2022.
//

import UIKit

struct Colors {
  
  static let highlighted = UIColor(named: "fa-highlighted")!
  static let white = UIColor(named: "fa-cultureWhite")!
  static let black = UIColor(named: "fa-black")!
}
