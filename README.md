# faceAnswer
Why use our fingers to control games if we can do it with our head moves, right? Boring, the conventional period is OVER!

faceAnswer is a trivia game that lets users answer questions by tilting their heads.

## Game Features

There are 35 questions in 5 different categories:
* Movies
* History
* Music
* Codeway
* Random

Each round consists of "10" questions. Users have 15 seconds to answer. If they don't *The truth will be revealed (uuu spooky)*. 

Also, don't forget to look "How to play?" section before starting to play. 

## Architecture
Clean Swift(a.k.a VIP) architecture is used in faceAnswers. Clean Swift is Uncle Bob’s Clean Architecture applied to iOS and Mac projects. The flow diagram of the architecture is like down below. 
![Source: the appspace.com](https://miro.medium.com/max/1384/1*QV4nxWPd_sbGhoWO-X7PfQ.png "VIP Architecture")

Source: the appspace.com

For more information:
* [Clean Swift Official Page](https://clean-swift.com/)
* [Medium Article](https://medium.com/hackernoon/introducing-clean-swift-architecture-vip-770a639ad7bf)

## Face Detection
To detect face there are multiple ways in Swift. In this project, I used the **Vision** framework. With Vision, I detect the face movement via yaw angle.
Vision Framework
* [Vision Framework](https://developer.apple.com/documentation/vision)

## Game Gif
* ![Medium Article](https://media.giphy.com/media/3X9ZW9LduPdF46J9Kh/giphy.gif)

